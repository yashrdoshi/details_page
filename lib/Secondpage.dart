import 'package:details_page/msg.dart';
import 'package:flutter/material.dart';

class DescriptionPage extends StatefulWidget {
  final String imgPath;

  DescriptionPage({this.imgPath});
  @override
  _DescriptionPageState createState() => _DescriptionPageState();
}

class _DescriptionPageState extends State<DescriptionPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(builder: (context) => MsgPage()));
        },
        isExtended: true,
        backgroundColor: Colors.green,
        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
        icon: Icon(Icons.message),
        label: Text('Contact Owner'),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      backgroundColor: Colors.purple[100],
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            backgroundColor: Colors.purple,
            pinned: true,
            primary: true,
            titleSpacing: 1.0,
            forceElevated: true,
            automaticallyImplyLeading: true,
            expandedHeight: 300.0,
            centerTitle: true,
            flexibleSpace: FlexibleSpaceBar(
              centerTitle: true,
              title: Text(
                "Red Tape Men's Formal Shoes",
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontSize: 17.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
            ),
          ),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                Container(
                  height: 300.0,
                  width: MediaQuery.of(context).size.width,
                  child: Padding(
                    padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 5.0),
                    child: Card(
                      color: Colors.white,
                      child: Image.asset("assets/shoes.png"),
                    ),
                  ),
                ),
                Container(
                  height: 300.0,
                  width: MediaQuery.of(context).size.width,
                  child: Padding(
                    padding: EdgeInsets.only(
                      left: 15.0,
                      right: 15.0,
                    ),
                    child: Card(
                      color: Colors.white,
                      child: Padding(
                        padding: EdgeInsets.only(
                            left: 15.0, right: 15.0, bottom: 5.0, top: 5.0),
                        child: ListView(
                          shrinkWrap: true,
                          physics: ClampingScrollPhysics(),
                          children: <Widget>[
                            Text(
                              "Details",
                              style: TextStyle(
                                  fontSize: 25.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.purple[700]),
                            ),
                            Divider(
                              height: 20.0,
                            ),
                            Text(
                              "Color : Brown",
                              style: TextStyle(fontSize: 20.0),
                            ),
                            Text(
                              "Size: 10",
                              style: TextStyle(fontSize: 20.0),
                            ),
                            Text(
                              "Outer Material: Leather",
                              style: TextStyle(fontSize: 20.0),
                            ),
                            Text(
                              "Other Details...",
                              style: TextStyle(fontSize: 20.0),
                            ),
                            Text(
                              ".........",
                              style: TextStyle(fontSize: 20.0),
                            ),
                            Text(
                              ".........",
                              style: TextStyle(fontSize: 20.0),
                            ),
                            Text(
                              ".........",
                              style: TextStyle(fontSize: 20.0),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                // BottomNavigationBar()
              ],
            ),
          )
        ],
      ),
    );
  }
}
