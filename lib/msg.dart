import 'package:flutter/material.dart';

class MsgPage extends StatefulWidget {
  @override
  _MsgPageState createState() => _MsgPageState();
}

class _MsgPageState extends State<MsgPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.purple[100],
      appBar: AppBar(
        backgroundColor: Colors.purple,
        leading: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Icon(Icons.person),
        ),
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "Mr.X",
              style: TextStyle(
                fontSize: 20.0,
              ),
            ),
            Divider(height: 5.0),
            Text(
              "+919855684363",
              style: TextStyle(
                fontSize: 15.0,
              ),
            ),
          ],
        ),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).pop();
              }),
          Icon(Icons.menu),
          SizedBox(
            width: 20.0,
          ),
        ],
      ),
      body: Stack(
        children: <Widget>[
          Positioned(
            top: 530,
            left: 20.0,
            child: Container(
              color: Colors.white,
              height: 50.0,
              width: MediaQuery.of(context).size.width * .9,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
