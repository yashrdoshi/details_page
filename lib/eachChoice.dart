import 'package:flutter/material.dart';


List selectedItems = [];
bool _isSelected = false;
bool _isSelectAll = false;


class BuildEachChoice extends StatefulWidget {
  final String name;
  BuildEachChoice({this.name});
  @override
  _BuildEachChoiceState createState() => _BuildEachChoiceState();
}

class _BuildEachChoiceState extends State<BuildEachChoice> {

  @override
  void initState() {
    super.initState();
    setState(() {
      
    });
  }
  @override
  Widget build(BuildContext context) {
    return FilterChip(
        label: Text(
          widget.name,
        ),
        labelStyle: TextStyle(
          color: _isSelected ? Colors.yellow[500] : Colors.black,
          fontSize: 30.0,
        ),
        backgroundColor: Colors.white,
        selected: _isSelected,
        disabledColor: Colors.red,
        pressElevation: 30.0,
        checkmarkColor: Colors.yellow[500],
        selectedColor: Colors.black,
        onSelected: (val) {
          setState(() {
            if (_isSelectAll) {
              val = _isSelectAll;
            } else {
              _isSelected = val;
            }
            // if(!val && _isEveryThingSelected){
            //   _isSelectAll = val;
            //   _isSelectAll = !val;
            // }
            if (val && selectedItems.contains(widget.name) == false) {
              selectedItems.add(widget.name);
            }
            if (!val) {
              if (selectedItems.contains(widget.name) == true) {
                selectedItems.remove("${widget.name}");
              }
            }
          });
          print(selectedItems);
        });
  }
}

class SelectAll extends StatefulWidget {
  final String name;
  SelectAll({this.name});
  @override
  _SelectAllState createState() => _SelectAllState();
}

class _SelectAllState extends State<SelectAll> {
  @override
  Widget build(BuildContext context) {
    return FilterChip(
        label: Text(
          widget.name,
        ),
        labelStyle: TextStyle(
          color: _isSelectAll ? Colors.yellow[500] : Colors.black,
          fontSize: 30.0,
        ),
        backgroundColor: Colors.white,
        selected: _isSelectAll,
        disabledColor: Colors.red,
        pressElevation: 30.0,
        checkmarkColor: Colors.yellow[500],
        selectedColor: Colors.black,
        onSelected: (val) {
          setState(() {
            // if(val){
            // _isEveryThingSelected = val;
            // }
            _isSelectAll = val;
            _isSelected = val;
            print("Value of val of Select All");
            print(val);
            print("Value of _isSlected");
            print(_isSelected);
            // Build();
          });
        });
  }
}
