import 'package:flutter/material.dart';

List selectedItems = [];
bool _isSelected = false;
bool _isSelectAll = false;

class Preference extends StatefulWidget {
  @override
  _PreferenceState createState() => _PreferenceState();
}

class _PreferenceState extends State<Preference> {
  @override
  void initState() {
    super.initState();
    // setState(() {
    //   print("Init Val of _isSelected = $_isSelected");
    //   _isSelected = false;
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Preferences"),
        backgroundColor: Colors.purple,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: ListView(
          children: <Widget>[
            Container(
              height: 40.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(20.0)),
                color: Colors.yellow[100],
              ),
              child: Center(
                child: Text("Choose your Preference to never miss Dicussions!"),
              ),
            ),
            SizedBox(
              height: 50.0,
            ),
            Container(
              height: 300.0,
              width: MediaQuery.of(context).size.width,
              child: Card(
                borderOnForeground: false,
                elevation: 20.0,
                color: Colors.yellow[500],
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: 80.0,
                        width: MediaQuery.of(context).size.width,
                        child: Card(
                          color: Colors.yellow[500],
                          elevation: 0.0,
                          child: Text(
                            "App Development",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 60.0,
                                fontWeight: FontWeight.bold,
                                fontFamily: "Signatra"),
                          ),
                        ),
                      ),
                      SelectAll(
                        name: "Select All",
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Container(
                        color: Colors.yellow[500],
                        child: Wrap(
                          spacing: 5.0,
                          runSpacing: 8.0,
                          children: <Widget>[
                            BuildEachChoice(
                              name: "Flutter",
                            ),
                            BuildEachChoice(
                              name: "Android",
                            ),
                            BuildEachChoice(
                              name: "React Native",
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }
}

class BuildEachChoice extends StatefulWidget {
  final String name;
  BuildEachChoice({this.name});
  @override
  _BuildEachChoiceState createState() => _BuildEachChoiceState();
}

class _BuildEachChoiceState extends State<BuildEachChoice> {
  @override
  Widget build(BuildContext context) {
    return InputChip(
        label: Text(
          widget.name,
        ),
        labelStyle: TextStyle(
          color: _isSelected ? Colors.yellow[500] : Colors.black,
          fontSize: 30.0,
        ),
        backgroundColor: Colors.white,
        selected: _isSelected,
        disabledColor: Colors.red,
        pressElevation: 30.0,
        checkmarkColor: Colors.yellow[500],
        selectedColor: Colors.black,
        onSelected: (val) {
          setState(() {
            if (_isSelectAll) {
              val = _isSelectAll;
            } else {
              _isSelected = val;
            }

            if (val && selectedItems.contains(widget.name) == false) {
              selectedItems.add(widget.name);
            }
            if (!val) {
              if (selectedItems.contains(widget.name) == true) {
                selectedItems.remove("${widget.name}");
              }
            }
          });
          print(selectedItems);
        });
  }
}

class SelectAll extends StatefulWidget {
  final String name;
  SelectAll({this.name});
  @override
  _SelectAllState createState() => _SelectAllState();
}

class _SelectAllState extends State<SelectAll> {
  @override
  Widget build(BuildContext context) {
    return FilterChip(
        label: Text(
          widget.name,
        ),
        labelStyle: TextStyle(
          color: _isSelectAll ? Colors.yellow[500] : Colors.black,
          fontSize: 30.0,
        ),
        backgroundColor: Colors.white,
        selected: _isSelectAll,
        disabledColor: Colors.red,
        pressElevation: 30.0,
        checkmarkColor: Colors.yellow[500],
        selectedColor: Colors.black,
        onSelected: (val) {
          setState(() {
            _isSelectAll = val;
            _isSelected = val;
            // Navigator.of(context)
            //     .push(new MaterialPageRoute(builder: (BuildContext context) {
            //   return new Preference();
            // }));
          });
        });
  }
}
